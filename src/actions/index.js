import axios from 'axios';

const API_KEY = '8c73d02f586c0aca845468e0a49d741c';
const ROOT_URL = `http://api.openweathermap.org/data/2.5/forecast?appid=${API_KEY}`;

export const FETCH_WEATHER = 'FETCH_WEATHER';

export const fetchWeather = (city) => {
  const url = `${ROOT_URL}&q=${city},lv`;
  const request = axios.get(url);

  // redux promise checks payload, if it is promise
  // it stops the actions, resolves request / promise and then
  // dispatches action
  return {
    type: FETCH_WEATHER,
    payload: request
  };
};
