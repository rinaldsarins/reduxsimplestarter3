import { FETCH_WEATHER } from '../actions';

const INITIAL_STATE = [];

export default (state = INITIAL_STATE, action) => {
  console.log(state);
  switch (action.type) {
    case FETCH_WEATHER:
      return [action.payload.data, ...state];

    default:
      return state;
  }
};
