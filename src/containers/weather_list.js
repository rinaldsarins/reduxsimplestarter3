import React, { Component } from 'react';
import { connect } from 'react-redux';
import Chart from '../components/chart';
import GoogleMap from '../components/google_map';

class WeatherList extends Component {
  renderList() {
    return this.props.weather.map(cityData => {
      const temps = cityData.list.map(item => item.main.temp - 273.5);
      const humidities = cityData.list.map(item => item.main.humidity);
      const pressures = cityData.list.map(item => item.main.pressure);
      const { lat, lon } = cityData.city.coord;

      return (
        <tr key={cityData.city.id}>
          <td><GoogleMap lat={lat} lon={lon} /></td>
          <td style={{ width: '30%' }}><Chart data={temps} color="red" units="C" /></td>
          <td style={{ width: '30%' }}><Chart data={pressures} color="purple" units="hPa" /></td>
          <td style={{ width: '30%' }}><Chart data={humidities} color="lightblue" units="%" /></td>
        </tr>
      );
    });
  }

  render() {
    return (
      <table className="table table-hover">
        <thead>
          <tr>
            <th>City</th>
            <th>Temperature (C)</th>
            <th>Pressure (hPa)</th>
            <th>Humidity (%)</th>
          </tr>
        </thead>
        <tbody>
          {this.renderList()}
        </tbody>
      </table>
    );
  }
}

const mapStateToProps = ({ weather }) => {
  return { weather };
};

export default connect(mapStateToProps)(WeatherList);
